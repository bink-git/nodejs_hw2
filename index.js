require('dotenv').config();

const express = require('express');
const app = express();

const connectDB = require('./src/db/connect');

const authenticateUser = require('./src/middleware/authentication');

const authRouter = require('./src/routes/auth');
const userRouter = require('./src/routes/users');
const noteRouter = require('./src/routes/notes');

app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/users', authenticateUser, userRouter);
app.use('/api/notes', authenticateUser, noteRouter);

const port = process.env.PORT || 8080;

const start = async () => {
  try {
    await connectDB(process.env.MONGO_URI);
    app.listen(port, () => {
      console.log(`Server is listening on port ${port}...`);
    });
  } catch (e) {
    console.log(e);
  }
};

start();
