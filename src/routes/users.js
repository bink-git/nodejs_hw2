const {Router} = require('express');
// eslint-disable-next-line new-cap
const router = Router();

const {
  getUser,
  deleteUser,
  changeUserPassword,
} = require('../controllers/users');

router.route('/me/').get(getUser).delete(deleteUser).patch(changeUserPassword);

module.exports = router;
