const {Router} = require('express');
// eslint-disable-next-line new-cap
const router = Router();

const {register, login} = require('../controllers/auth');

router.post('/register', register);
router.post('/login', login);

module.exports = router;
