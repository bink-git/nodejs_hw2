const {Router} = require('express');
// eslint-disable-next-line new-cap
const router = Router();

const {
  getNotes,
  checkNote,
  getNoteById,
  updateNote,
  deleteNote,
  createNote,
} = require('../controllers/notes');

router.route('/').get(getNotes).post(createNote);
router
    .route('/:id')
    .get(getNoteById)
    .put(updateNote)
    .patch(checkNote)
    .delete(deleteNote);

module.exports = router;
