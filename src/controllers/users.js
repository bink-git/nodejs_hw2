const User = require('../models/User');
const Credentials = require('../models/Credentials');

const bcrypt = require('bcryptjs');

const getUser = async (req, res) => {
  const { userId, username } = req.user;

  try {
    const user = new User({
      _id: userId,
      username,
    });

    await user.save();
    res.status(200).json({ user });
  } catch (e) {
    res.status(400).json({
      message: 'Failed to get data',
    });
  }
};

const deleteUser = async (req, res) => {
  const { userId } = req.user;

  const deleteUser = User.findOneAndDelete({ userId });

  if (deleteUser) {
    res.status(200).json({
      message: 'Success',
    });
  }

  res.status(400).json({
    message: 'Failed to delete user',
  });
};

const changeUserPassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const userId = req.user.userId;

  try {
    const user = await Credentials.findOne({ _id: userId });

    const isMatch = await bcrypt.compare(
      String(oldPassword),
      String(user.password)
    );

    if (isMatch) {
      // eslint-disable-next-line no-unused-vars
      const user = await Credentials.findOneAndUpdate(
        { _id: userId },
        { password: await bcrypt.hash(newPassword, 12) },
        {
          new: true,
        }
      );

      res.status(200).json({
        message: 'Success',
      });
    }

    res.status(400).json({
      message: "Old password wasn't matched",
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

module.exports = { getUser, deleteUser, changeUserPassword };
