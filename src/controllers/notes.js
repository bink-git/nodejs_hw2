const Note = require('../models/Note');

const getNotes = async (req, res) => {
  console.log(req.user.userId);

  try {
    const notes = await Note.find({userId: req.user.userId}, '-__v');

    res.status(200).json({notes});
  } catch (e) {
    res.status(400).json({
      message: e.message,
    });
  }
};

const createNote = async (req, res) => {
  const text = req.body.text;

  try {
    const note = new Note({
      userId: req.user.userId,
      text,
    });

    await note.save();

    res.status(200).json({
      message: 'Success',
    });
  } catch (e) {
    res.status(400).json({
      message: e.message,
    });
  }
};

const getNoteById = async (req, res) => {
  const noteId = req.params.id;

  try {
    const note = await Note.findOne({userId: req.user.userId, _id: noteId});

    res.status(200).json({note});
  } catch (e) {
    res.status(400).json({
      message: e.message,
    });
  }
};

const updateNote = async (req, res) => {
  const noteId = req.params.id;
  const text = req.body.text;

  try {
    // eslint-disable-next-line no-unused-vars
    const note = await Note.findOneAndUpdate(
        {userId: req.user.userId, _id: noteId},
        {text},
        {
          new: true,
        },
    );

    res.status(200).json({message: 'Success'});
  } catch (e) {
    res.status(400).json({
      message: e.message,
    });
  }
};

const checkNote = async (req, res) => {
  const noteId = req.params.id;

  try {
    // eslint-disable-next-line no-unused-vars
    const note = await Note.findOneAndUpdate(
        {userId: req.user.userId, _id: noteId},
        {completed: true},
        {
          new: true,
        },
    );

    res.status(200).json({message: 'Success'});
  } catch (e) {
    res.status(400).json({
      message: e.message,
    });
  }
};

const deleteNote = async (req, res) => {
  const noteId = req.params.id;

  try {
    // eslint-disable-next-line no-unused-vars
    const note = await Note.findOneAndDelete({
      userId: req.user.userId,
      _id: noteId,
    });

    res.status(200).json({message: 'Success'});
  } catch (e) {
    res.status(400).json({
      message: e.message,
    });
  }
};

module.exports = {
  getNotes,
  checkNote,
  createNote,
  deleteNote,
  updateNote,
  getNoteById,
};
