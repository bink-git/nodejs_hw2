const Credentials = require('../models/Credentials');

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const register = async (req, res) => {
  const { username, password } = req.body;

  const user = new Credentials({
    username,
    password: await bcrypt.hash(password, 12),
  });

  await user.save();

  res.status(200).json({
    message: 'Success',
  });

  if (!(await user.save())) {
    res.status(400).json({
      message: 'Credentials error',
    });
  }
};

const login = async (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    res.status(400).json({ message: 'Credentials error' });
  }

  try {
    const currentUser = await Credentials.findOne({ username });

    // passwords is matches
    if (bcrypt.compareSync(password, currentUser.password)) {
      const payload = {
        userId: currentUser._id,
        username: currentUser.username,
        password: currentUser.password,
      };
      const token = jwt.sign(payload, process.env.JWT_SECRET);

      console.log(token);

      return res.status(200).json({
        message: 'Success',
        jwt_token: token,
      });
    }

    return res.status(400).json({
      message: 'Passwords do not match',
    });
  } catch (err) {
    return res.status(400).json({ message: 'Not authorized' });
  }
};

module.exports = {
  register,
  login,
};
