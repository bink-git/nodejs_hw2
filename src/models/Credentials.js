const mongoose = require('mongoose');

const CredentialSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, 'Enter username'],
    minlength: 3,
    maxlength: 50,
  },
  password: {
    type: String,
    required: [true, 'Enter password'],
    minlength: 6,
  },
});

module.exports = mongoose.model('Credentials', CredentialSchema);
